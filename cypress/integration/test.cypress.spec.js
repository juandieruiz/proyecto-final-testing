describe('Test - Crear y Borrar Tareas', () => {

    beforeEach(() => {
        cy.visit('/');
    });


    it('Crea una Tarea', () => {
        cy.get('#id').type(5);
        cy.get('#user_id').type(500);
        cy.get('#title').type("Crear tarea en Cypress");
        cy.get('#due_on').type("2021-09-30");
        cy.get('#status').select('pending');
        cy.get('#send').click();
    });


    it('Elimina una Tarea', () => {
        cy.get('#delete').click();
    });

});
