import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
// import { ListaTareasComponent } from './components/lista-tareas/lista-tareas.component';

describe('AppComponent', () => {

  // Declaramos las variables que vamos a usar
  let fixture: ComponentFixture<AppComponent>;
  let app: AppComponent;

  // Ejecutar líneas de código previas a cada uno de los IT
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule
      ]
    }).compileComponents();

    // Inicializamos las variables fixture y app
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.componentInstance;

  });

  it('Comprobar que se puede instanciar un objeto AppComponent (app)', () => {
    // Comprobar que se puede instanciar un objeto AppComponent
    expect(app).toBeTruthy();
  });

  it('Comprobar que la propiedad title dentro de app.component = angular-testing-front', () => {
    // Comprobamos que la propiedad title dentro de la clase app.component.ts
    // tiene un valor de "angular-testing-front"
    expect(app.title).toEqual('angular-testing-front');
  });

  // Shallow Testing: Pruebas sobre la vista del componente
  it('Comprobar título del toolbar', () => {
    fixture.detectChanges();
    
    const compiled: HTMLElement = fixture.nativeElement as HTMLElement;
    
    expect(compiled.querySelector('.toolbar span')?.textContent).toContain('Proyecto Final - Testing');
  });
});
