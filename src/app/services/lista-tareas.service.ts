import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ListaTareasService {

  constructor(private _http: HttpClient) { }

  // agregarTarea(tarea: any): Observable<any> {
  //   return this._http.post('https://gorest.co.in/public/v1/users/123/todos?access-token=a21493171b8dff2d266103161e18e92ea41a0ca9552e1d06a4f67e1d5729d8ec', tarea);
  // }

  // Traer la lista de contactos
  getTareas(): Observable<any> {
    return this._http.get('https://gorest.co.in/public/v1/todos?page=1');
  }

}
