import { Observable, of } from 'rxjs';


export class ListaTareasServiceStub {
  // Simulamos lo que devuelve el método getTareas
    getTareas(): Observable<any> {
        return of({
            data: [
              {
              "id": 115,
              "user_id": 87,
              "title": "Verbera vado incidunt vespillo curo autem spectaculum teneo voluptatibus.",
              "due_on": "2021-10-15T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 116,
              "user_id": 87,
              "title": "Deduco spiritus creptio coaegresco templum decretum theologus vinitor defluo blandior.",
              "due_on": "2021-10-02T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 120,
              "user_id": 91,
              "title": "Canis conscendo pectus avoco eius amicitia atavus suspendo laudantium.",
              "due_on": "2021-10-03T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 121,
              "user_id": 92,
              "title": "Thymum patior temeritas venia alo vallum.",
              "due_on": "2021-10-20T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 122,
              "user_id": 92,
              "title": "Venustas patior cerno debitis theologus caritas damnatio creber.",
              "due_on": "2021-10-23T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 123,
              "user_id": 93,
              "title": "Velit tempus bonus tertius enim conor aestus carbo aspernatur.",
              "due_on": "2021-10-16T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 124,
              "user_id": 93,
              "title": "Ago tollo pariatur volaticus creta utrimque amicitia claudeo ait debitis solio.",
              "due_on": "2021-10-21T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 125,
              "user_id": 93,
              "title": "Qui delego cognomen adipiscor cilicium vivo abstergo depromo auxilium trans.",
              "due_on": "2021-10-08T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 126,
              "user_id": 94,
              "title": "Id abbas sono adeo acies.",
              "due_on": "2021-10-02T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 127,
              "user_id": 94,
              "title": "Error tamquam tubineus aperiam audax amplexus textor volo corroboro.",
              "due_on": "2021-10-07T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 128,
              "user_id": 95,
              "title": "Celer ab territo triduana vomer tyrannus viduo comminor.",
              "due_on": "2021-10-19T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 129,
              "user_id": 95,
              "title": "Uredo caterva auctus acerbitas creo acidus.",
              "due_on": "2021-10-15T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 130,
              "user_id": 95,
              "title": "Eaque eos copiose tabula decerno ara deinde.",
              "due_on": "2021-10-20T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 131,
              "user_id": 96,
              "title": "Umerus cenaculum verumtamen clementia hic qui celo ustilo.",
              "due_on": "2021-10-08T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 132,
              "user_id": 97,
              "title": "Qui torrens arguo molestiae repudiandae aut supellex turba.",
              "due_on": "2021-10-13T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 133,
              "user_id": 97,
              "title": "Avoco terror cura et aduro verbera supplanto trepide volup sapiente.",
              "due_on": "2021-09-30T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 134,
              "user_id": 98,
              "title": "Cruciamentum texo contra et utor curis vitiosus omnis armarium.",
              "due_on": "2021-10-15T00:00:00.000+05:30",
              "status": "completed"
              },
              {
              "id": 135,
              "user_id": 99,
              "title": "Consectetur velit currus demonstro saepe admoneo.",
              "due_on": "2021-10-15T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 136,
              "user_id": 100,
              "title": "Accusator cubo catena culpo aliqua.",
              "due_on": "2021-10-19T00:00:00.000+05:30",
              "status": "pending"
              },
              {
              "id": 137,
              "user_id": 100,
              "title": "Tumultus accendo facilis bellum cogo voluptates.",
              "due_on": "2021-10-04T00:00:00.000+05:30",
              "status": "completed"
              }
                ]
        });
    }

}
