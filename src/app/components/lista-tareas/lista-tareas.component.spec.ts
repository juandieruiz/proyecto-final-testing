import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';

// Propios
import { ListaTareasService } from 'src/app/services/lista-tareas.service';
import { ListaTareasServiceStub } from 'src/app/services/mocks/lista-tareas.service.mock';
import { ListaTareasComponent } from './lista-tareas.component';

describe('ListaTareasComponent', () => {
  let fixture: ComponentFixture<ListaTareasComponent>;
  let component: ListaTareasComponent;

  let tareaTest:{
    id: 100,
    user_id: 10,
    title: "Tarea de Test",
    due_on: "2021-09-22T00:00:00.000+00:00",
    status: "pending"
  };

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaTareasComponent ],
      imports: [
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: ListaTareasService,
          useClass: ListaTareasServiceStub
        }
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaTareasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


  it('Debe llamar a getTareas del Servicio en ngOnInit', () => {
    // Ponemos un espía a contar las veces que se llama al método
    spyOn(component.tareasService, 'getTareas').and.callThrough();

    // forzamos una llamada al método ngOnInit() del componente
    component.ngOnInit();

    // Verificamos que se haya llamado a getTareas() una vez
    expect(component.tareasService.getTareas).toHaveBeenCalledTimes(1);
  });


  it('Debe obtener la lista de tareas y guardarla', () => {

    fixture.whenStable().then(() => {
      // Cuando ya está estable, ya debería tener el listado de alumnos
      // Verificamos que no está vacía, es decir que su logitud es mayor que 0
      expect(component.tareas.length).toBeGreaterThan(0);
    })
  });


  it('Debe crear nueva tarea', () => {

    let numTareas = component.tareas.length;

    expect(component.tareas.length).toBe(numTareas);

    component.agregarTarea();

    expect(component.tareas.length).toBeGreaterThan(numTareas);

  });


  it('Debe eliminar una tarea', () => {

    component.agregarTarea();

    let numTareas = component.tareas.length;

    expect(component.tareas.length).toBe(numTareas);

    component.eliminarTarea(tareaTest);

    expect(component.tareas.length).toBeLessThan(numTareas);

  });


  it('Debería renderizar la lista de tareas en el HTML', () => {
    fixture.whenStable().then(() => {
      // Nos aseguramos de que la vista se actualice
      fixture.detectChanges()
      
      const elemento = fixture.debugElement.query(By.css('.tareas')).nativeElement;

      expect(elemento.childNodes[1]).not.toBeNull();
    })
  });

});
